package com.mygdx.game.animation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.ScreenUtils;
import com.mygdx.game.screen.KillInCold;

public class EquipPresentations implements Screen {

    Texture equip;
    final KillInCold kill;
    OrthographicCamera camera;

    public EquipPresentations(final KillInCold kill){
        equip = new Texture(Gdx.files.internal("Background/equip.jpg"));
        this.kill = kill;
        camera = new OrthographicCamera();
        camera.setToOrtho(false,1000,1000);
    }

    public  void draw(){
        ScreenUtils.clear(0,0,0.2f,1);
        camera.update();
        kill.batch.setProjectionMatrix(camera.combined);
        kill.batch.begin();

        kill.batch.draw(equip,0,0,Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        kill.batch.end();
    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        equip.dispose();
        kill.dispose();
    }
}
