package com.mygdx.game.animation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.mygdx.game.levels.ShowLevel;
import com.mygdx.game.screen.KillInCold;
import com.mygdx.game.screen.KillScreen;

public class MainKillScreen implements Screen {
    final KillInCold kill;
    private boolean isButtonPressed;
    OrthographicCamera camera;
    Texture buttonPlay;
    Texture buttonExit;
    Texture equips;
    private Timer timer;
    private boolean activeKill;
    private ShowLevel showLevel;
    EquipPresentations equipPresentations;
    public MainKillScreen(final KillInCold kill){
        isButtonPressed = false;

        this.kill = kill;
        equipPresentations = new EquipPresentations(kill);
        timer = new Timer();
        activeKill = false;
        showLevel = new ShowLevel(KillScreen.currentLevel);
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 1000, 1000);
        buttonPlay = new Texture(Gdx.files.internal("menu/play.png"));
        buttonExit = new Texture(Gdx.files.internal("menu/exit.png"));
        equips = new Texture(Gdx.files.internal("menu/buttons.png"));

    }

    public void render(float delta) {
        ScreenUtils.clear(0, 0, 0.2f, 1);
        camera.update();
        kill.batch.setProjectionMatrix(camera.combined);
        kill.batch.begin();

        Texture backgroundTexture = new Texture(Gdx.files.internal("menu/kill.jpg"));
        kill.batch.draw(backgroundTexture, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        // Affichage du bouton au milieu de l'écran
        float buttonX = (Gdx.graphics.getWidth() - buttonPlay.getWidth()) / 2f;
        float buttonY = (Gdx.graphics.getHeight() - buttonPlay.getHeight()) / 2f;
        kill.batch.draw(buttonPlay, buttonX, buttonY);

        // Placement du bouton Exit au-dessus du bouton Play
        float buttonExitX = buttonX - 35;
        float buttonExitY = buttonY + buttonPlay.getHeight() + 1;
        kill.batch.draw(buttonExit, buttonExitX, buttonExitY);

        // Placement du bouton qui affiches les developpeurs de ce jeu en dessous du bouton Play
        float buttonStatX = buttonX - 40;
        float buttonStatY = buttonY - equips.getHeight() - 1;
        kill.batch.draw(equips, buttonStatX, buttonStatY);
        kill.font.setColor(Color.WHITE); // Définit la couleur jaune
        kill.font.getData().setScale(2f);
        kill.font.draw(kill.batch, "LES DEVS GOATS", buttonStatX + 35, buttonStatY + equips.getHeight() - 70);
        kill.batch.setColor(Color.WHITE); // Rétablit la couleur blanche par défaut


        kill.font.draw(kill.batch, "Welcome to Kill in Cold!!!", 100, 150);
        kill.font.setColor(Color.RED);
        kill.batch.end();

        if (activeKill) {
            showLevel.draw();
        }

        if (Gdx.input.isTouched()) {
            int touchX = Gdx.input.getX();
            int touchY = Gdx.graphics.getHeight() - Gdx.input.getY();
            //Ecouter l'evenement du bouton et lancer le jeu après 3 seconde
            if (touchX >= buttonX && touchX < buttonX + buttonPlay.getWidth()
                    && touchY >= buttonY && touchY < buttonY + buttonPlay.getHeight()) {
                isButtonPressed = true;
            }
            else if (touchX >= buttonExitX && touchX < buttonExitX + buttonExit.getWidth()
                    && touchY >= buttonExitY && touchY < buttonExitY + buttonExit.getHeight()) {
                Gdx.app.exit(); // Quitter l'écran lorsque le bouton "Exit" est cliqué

                //Cette partie permetra d'afficher l'equipe qui à conçu ce jeu
            } else if (touchX >= buttonStatX && touchX < buttonStatX + equips.getWidth()
                    && touchY >= buttonStatY && touchY < buttonStatY + equips.getHeight()) {
                    equipPresentations.draw();
            }

        } else {
            if (isButtonPressed) {
                showLevelMessage();
                isButtonPressed = false;
            }
        }
    }


    /**
     * Permet d'afficher le level du commencement
     * avec un timer de 3 seconde et puis relance KillScreen
     */
    private void showLevelMessage() {
        ScreenUtils.clear(0, 0, 0.2f, 1); // Efface l'écran et affiche le fond gris

        activeKill = true;
        showLevel = new ShowLevel(KillScreen.currentLevel);
        showLevel.draw();
        timer.scheduleTask(new Task() {
            @Override
            public void run() {
                activeKill = false;
                showLevel.hide();
                kill.setScreen(new KillScreen(kill));
            }
        }, 3); // Durée en secondes du message du niveau

        timer.start();
    }


    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        kill.batch.dispose();
        kill.font.dispose();
        buttonPlay.dispose();
    }

    @Override
    public void show(){}

}
