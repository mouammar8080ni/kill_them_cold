package com.mygdx.game.exceptions;

public class ImageNullException extends Exception{

    public ImageNullException() {
        super("pas d'image trouvé");
    }
}
