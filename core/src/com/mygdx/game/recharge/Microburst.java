package com.mygdx.game.recharge;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.actors.Target;

public class Microburst extends Recharge {

    private static String DEFAULT_NAME = "Microburst";
    private static int strength = 10;
    public static final String TEXTURE_FILE_NAME = "Recharge/little.png";
    Texture texture = new Texture(Gdx.files.internal(TEXTURE_FILE_NAME));
    public Rectangle shape;

    public Microburst() {
        super(strength, DEFAULT_NAME);
        shape = new Rectangle(0, 0, 100-texture.getWidth(), texture.getHeight());
    }

    public void draw(SpriteBatch batch) {
        batch.draw(texture, shape.x, shape.y, shape.width, shape.height);
    }


}
