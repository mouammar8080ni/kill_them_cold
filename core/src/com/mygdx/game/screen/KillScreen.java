package com.mygdx.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.actors.*;
import com.mygdx.game.ammunition.Fusils;
import com.mygdx.game.animation.AudioMusic;
import com.mygdx.game.animation.KillFinish;
import com.mygdx.game.ammunition.Projectiles;
import com.mygdx.game.exceptions.ImageNullException;
import com.mygdx.game.levels.*;
import com.mygdx.game.ammunition.Ball;
import com.mygdx.game.recharge.Microburst;
import com.mygdx.game.recharge.Thunderclap;
import com.mygdx.game.recharge.Titanstrike;


import java.util.ArrayList;
import java.util.Iterator;

public class KillScreen implements Screen {

    ShowLevel message;
    public static float deltas= MathUtils.random(-5,5);//vitesse
    private final KillInCold kill;
    private OrthographicCamera camera;
    private Background background;
    private static AudioMusic audioMusic;
    private static Attacker attacker;
    public static ArrayList<Ball> balls = new ArrayList<>();// Liste de balles
    public static ArrayList<Microburst> microbursts = new ArrayList<>();// Liste de recharges petit
    public static ArrayList<Thunderclap> thunderclaps = new ArrayList<>();//recharge moyen
    public static ArrayList<Titanstrike> titanstrikes= new ArrayList<>();//recharge moyen
    public static Array<TomCat> tomCats; // petit enemies
    public static Array<BlackHawk> blackHawks;// enemie lourds
    public static ArrayList<Ball> ball_boss; // Genneration du Boss chef de bande
    SpriteBatch batch;
    ShapeRenderer shapeRenderer;
    BitmapFont font;
    public static long lastKillTime;
    public static long lastTime = TimeUtils.millis();
    private static int  count=0;
    TomCat target;
    Microburst microburst;
    public static int scoreAttacker = 0;
    private static boolean isGameOver;//verifie si l'etat de jeu si cest Win ou Lost
    KillFinish killFinish;
    public int tmp = 0;
    public static int currentLevel = LevelOne.LEVEL;
    private boolean isShowingMessage = false;
    private static Array<Projectiles> projectiles;
    private static Array<Fusils> fusils;
    private boolean isPKeyPressed = false;
    private boolean isRightMouseButtonPressed = false;

    private boolean isPKeyLeftPressed = false;
    private boolean isLeftMouseButtonPressed = false;
    HigherLevel higherLevel;
    Boss boss;

    public KillScreen(final KillInCold kill){
        shapeRenderer = new ShapeRenderer();
        batch = new SpriteBatch();
        font = new BitmapFont();
        message = new ShowLevel(2);
        killFinish = new KillFinish();
        higherLevel = new HigherLevel(currentLevel);
        boss = new Boss(Boss.PATH);

        audioMusic = new AudioMusic("audio/music.mp3");
        attacker = new Attacker(Attacker.DEFAULT_NAME);
        target = new TomCat(TomCat.PATH);
        microburst = new Microburst();

        this.kill = kill;
        this.kill.addScreen(this);
        isGameOver = false;

        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800,400);
        background = new Background(0,0,"Background/ground.jpeg");
        tomCats = new Array<TomCat>();
        blackHawks =new Array<BlackHawk>();
        microbursts = new ArrayList<>();
        projectiles = new Array<Projectiles>();
        ball_boss = new ArrayList<>();
        fusils = new Array<Fusils>();
        count = (int)attacker.shape.y + 10;

    }

    @Override
    public void render(float delta) {
        ScreenUtils.clear(0, 0, 0.2f, 1);
        if (isShowingMessage) {
            this.nexLevel();
        } else {
            this.winLost(delta);

            // si le joue sa joue
            if (!isGameOver) {
                attacker.generateRecharge();
                //attacker.moveWithMouse();//Deplacement avec la souris
                attacker.moveWithTouch();// Deplacement avec les touche de directions

                //appels des fonctions de colision
                this.generateDifferentLevel();
                this.checkCollisionsTargets(blackHawks);
                this.checkCollisionsTargets(tomCats);
                this.checkColisionBoss();
                this.checkCollisionPorjectile();
                this.checkCollisionFusilsToAttacker();
                attacker.checkCollisionRechargeMicroburst();

                camera.update();
                kill.batch.setProjectionMatrix(camera.combined);
                batch.begin();
                shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
                background.draw(batch, delta);


                this.generateAttacker();
                //this.generateBallsBoss();
                AudioMusic.play();
                try {
                    attacker.draw(batch);
                } catch (ImageNullException e) {
                    e.printStackTrace();
                }

                try {
                    this.generateDifferentTarget();
                } catch (ImageNullException e) {
                    e.printStackTrace();
                }

                this.generateDifferentRecharge();
                attacker.updateBalle(batch, tomCats);
                attacker.updateBalle(batch, blackHawks);

                this.showStatsKill();
                this.throwProjectiles();
                this.throwFusils();
                this.generateProjectionFromAttacker();
                this.generateFusilToBoss();
                batch.end();
                shapeRenderer.end();
            }

            //si c'est perdu
            if (isGameOver) {
                batch.begin();
                killFinish.draw(batch);
                AudioMusic.stop();
                Background.setAnimationEnabled(false); //stopper l'animation
                batch.end();
            }
        }

    }


    /**
     * cette methode permet de recommencer le jeu
     * si l'acteur à perdu
     */
    public static void restartGame() {
        scoreAttacker = 0;
        isGameOver = false;
        blackHawks.clear();
        tomCats.clear();
        balls.clear();
        microbursts.clear();
        projectiles.clear();
        AudioMusic.dispose();
        Background.setAnimationEnabled(true);
        // Réinitialiser l'attaquant
        attacker = new Attacker(Attacker.DEFAULT_NAME);

        // Réinitialiser le niveau
        currentLevel = LevelOne.LEVEL;
        LevelOne.setActive(true);
        LevelTwo.setActive(false);
    }


    /**
     * une methode generecite qui permet de
     * faire la colision des differents enemies
     * avecs les balles de l'acteur
     * @param list
     */
    private void checkCollisionsTargets(Array<? extends Target> list) {
        Iterator<? extends Target> targetIter = list.iterator();
        while (targetIter.hasNext()) {
            Target target = targetIter.next();
            Iterator<Ball> ballIter = balls.iterator();

            //Faire sortir le petit enemie TomCat s'il arrive en bas de l'ecran
            if (target instanceof TomCat  && target.shape.y <= 0) {
                targetIter.remove();
            }
            while (ballIter.hasNext()) {
                Ball ball = ballIter.next();
                if (Intersector.overlaps(ball.getCirlce(), target.shape)) {
                    target.setLife(target.getLife() - 5);
                    ballIter.remove();
                    //verifie le type de l'enemie et si sa vie et a zero de le tuer et impacte de balles
                    if ((target instanceof TomCat || target instanceof BlackHawk) && target.getLife() <= 0) {
                        System.out.println(target.getLife());
                        scoreAttacker++;
                        targetIter.remove();
                    }
                    break;
                }
            }
        }
    }

    /**
     * Colision du boss
     */

    public void checkColisionBoss(){
        Iterator<Ball> ballIterator = ball_boss.iterator();
        while (ballIterator.hasNext()) {
            Ball ball = ballIterator.next();
            ball.update();
            ball.draw(batch);
            if(ball.getY()<0){
                ballIterator.remove();
            }
            if(Intersector.overlaps(ball.getCirlce(),attacker.shape)) {
                scoreAttacker++;
                ballIterator.remove();
            }

        }
    }

    //affichage du gagan ou du perdant
    public void winLost(float delta) {
        if (scoreAttacker == attacker.getMaxScore()) {
            isGameOver = true;
            // Afficher le message de victoire
            camera.update();
            kill.batch.setProjectionMatrix(camera.combined);

            batch.begin();
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

            background.draw(batch, delta);

            String message = "L'acteur " + Attacker.DEFAULT_NAME + " a gagné !";
            float x = Gdx.graphics.getWidth() / 2f -message.length() * 5;
            float y = Gdx.graphics.getHeight() / 2f - 60;
            font.getData().setScale(2f);
            font.setColor(Color.YELLOW);
            font.draw(batch, message, x, y);

            batch.end();
            shapeRenderer.end();
        } else if (attacker.getLife() <= 0) {
            isGameOver = true;
            // Afficher le message de défaite
            camera.update();
            kill.batch.setProjectionMatrix(camera.combined);

            batch.begin();
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

            background.draw(batch, delta);
            font.getData().setScale(2f);
            font.setColor(Color.RED);
            String message = "L'énémie " + target.getName() + " a gagné !";
            float x = Gdx.graphics.getWidth() / 2f - message.length() * 5;
            float y = Gdx.graphics.getHeight() / 2f -60;
            font.draw(batch, message, x, y);
            font.setColor(Color.RED);
            batch.end();
            shapeRenderer.end();
        }
    }

    //Permet le passage au Level
    public void nexLevel() {
        message.draw();

        if (TimeUtils.timeSinceMillis(lastKillTime) >= 5000) {
            isShowingMessage = false;
            message.hide();
            scoreAttacker = scoreAttacker + tmp;
            if (currentLevel == LevelOne.LEVEL) {
                LevelOne.createLittleTarget();
            } else if (currentLevel == LevelTwo.LEVEL) {
                LevelTwo.newFauconTarget(); // Passage au niveau suivant
            }
        }
    }

    /**
     * Generer les differents levels
     * et les differents enemies et tires de chaques levels
     */
    public void generateDifferentLevel(){
        if (TimeUtils.nanoTime() - KillScreen.lastKillTime > 1000000000) {
            if (currentLevel == LevelOne.LEVEL) {
                if (scoreAttacker >= 20) { // Passage au niveau Two
                    currentLevel = LevelTwo.LEVEL;
                    LevelTwo.setActive(true);
                    LevelOne.setActive(false);
                    isShowingMessage = true;
                    lastKillTime = TimeUtils.millis();
                }
                else if (scoreAttacker >= 40) {
                    currentLevel = HigherLevel.LEVEL;  // Passage au niveau Higher
                    // Effectuer des actions spécifiques au niveau 3 si nécessaire
                } else {
                    if (currentLevel == LevelOne.LEVEL && count % 20 == 0) {
                        LevelOne.createLittleTarget();
                    }
                }
            } else if (currentLevel == LevelTwo.LEVEL) {
                if (count % 20 == 0) {
                    LevelOne.createLittleTarget();
                    LevelTwo.newFauconTarget();
                } else if (count % 20 == 0) {
                    LevelTwo.newFauconTarget();
                }
            }

        }

    }

    /**
     * Generation de l'acteur avecs des tires diferents selon
     * le niveau
     */
    public void generateAttacker(){
        count = count + 1;
        if (count % 15 == 0) {
            Ball ball1 = new Ball(attacker.shape.x + attacker.shape.width / 2, attacker.shape.y + attacker.shape.height, 5, 5);
            balls.add(ball1);
            if(currentLevel >= LevelTwo.LEVEL) {
                attacker.multidirectionalBalls();//Ball mutidirectionnel a partir de nivaux 2
            }
        }

        if (count == Gdx.graphics.getHeight()) {
            count = 0;
        }

        Iterator<Ball> ballIter = balls.iterator();
        while(ballIter.hasNext()) {
            Ball ball = ballIter.next();
            ball.draw(batch);
            ball.update();
        }
    }

    //Generations des differents types d'enemies
    public void generateDifferentTarget() throws ImageNullException {

        // Little Target
        for (TomCat target : tomCats) {
             target.moveTargets(deltas);
            if (count % 20 == 0) {
                target.target_Ball();
            }

            target.checkCollisionAttacker(batch, attacker);
            target.draw(batch);
        }

        // Le grand Boss de Bande au niveau 3
        boss.moveTargets(deltas);
        if(scoreAttacker >= 55) {
            LevelOne.setActive(false);
            LevelTwo.setActive(false);
            if (count % 30 == 0) {
                boss.target_Ball();
                boss.checkColisionBossToAttacker(batch,attacker);
            }

            boss.draw(batch);
        }

        if(count>Gdx.graphics.getHeight()){
            count=0;
        }

        // un target Moyen au niveau 2
        for (BlackHawk target : blackHawks) {
              target.moveTargets(deltas);
            if (count % 20 == 0) {
                target.target_Ball();
            }
            target.checkCollisionAttacker(batch, attacker);
            target.draw(batch);
        }
    }

    public void generateBallsBoss(){
            count = count + 1;
            if (count % 15 == 0) {
                Ball ball1 = new Ball(boss.shape.x + boss.shape.width / 2, boss.shape.y + boss.shape.height, 5, 5);
                balls.add(ball1);
                if(currentLevel == LevelTwo.LEVEL) {
                   boss.multidirectionalBalls();//Ball mutidirectionnel a partir de nivaux 2
                }
            }

            if (count == Gdx.graphics.getHeight()) {
                count = 0;
            }

            Iterator<Ball> ballIter = balls.iterator();
            while (ballIter.hasNext()) {
                Ball ball = ballIter.next();
                ball.draw(batch);
                ball.update();
            }

    }

    // Generations des differents Reharges
    public void generateDifferentRecharge(){
        for (Microburst microburst1 : microbursts) {
            microburst1.draw(batch);
        }

        for(Thunderclap thunderclap : thunderclaps){
            thunderclap.draw(batch);
        }

    }

    /**
     * Permet de genrer une arme lourd pour l'acteur
     * et le lancer soit avec la touche P ou la touche
     * Droit de la souris
     */
    public void generateProjectionFromAttacker() {
        if(currentLevel >=LevelTwo.LEVEL) {
        //verifie quel bouton à eté touchée
        if (isPKeyPressed || isRightMouseButtonPressed) {
            // Lancement du missile
            Projectiles missile = new Projectiles(Projectiles.PATH, attacker.shape.x, 0, 20);
            projectiles.add(missile);
        }
        //Lancement des missiles
            for (Projectiles p : projectiles) {
                p.draw(batch);
                p.moveProjectile(1);
            }
        }

    }


    /**
     * Permet de genrer une arme lourd pour l'acteur
     * et le lancer soit avec la touche P ou la touche
     * Droit de la souris
     */
    public void generateFusilToBoss() {
        if(currentLevel >=LevelOne.LEVEL) {
            //verifie quel bouton à eté touchée
            if (isPKeyLeftPressed || isLeftMouseButtonPressed) {
                // Lancement du missile
                Fusils fusils1 = new Fusils(Fusils.PATH,attacker.shape.x,0,20);
                fusils.add(fusils1);
            }
            //Lancement des missiles
            for (Fusils fusil : fusils) {
                fusil.draw(batch);
                fusil.moveFusil(1);
            }
        }

    }

    //Detection des colision des missiles tiré par l'acteur
    public void checkCollisionPorjectile() {
        Iterator<Projectiles> projectile = projectiles.iterator();
        Iterator<TomCat> tomCatIterator = tomCats.iterator();
        Iterator<BlackHawk> blackHawkIterator = blackHawks.iterator();
        while (tomCatIterator.hasNext()) {
            TomCat tomCat = tomCatIterator.next();
            //BlackHawk blackHawk = blackHawkIterator.next();
            while (projectile.hasNext()) {
                Projectiles projectile1 = projectile.next();
                if (Intersector.overlaps(projectile1.shape, tomCat.shape)) {
                        projectile.remove();
                       tomCat.setLife(tomCat.getLife()-5);
                     //  blackHawk.setLife(blackHawk.getLife()-5);
                       if(tomCat.getLife() <=0){
                           scoreAttacker +=5;
                           tomCatIterator.remove();
                          // blackHawkIterator.remove();
                           break;
                       }
                }
            }
        }
    }

    /**
     * Permet de lancer le projectile soit par le bouton droit de la souris
     * soit en appuyant sur la touche P
     */
    public void throwProjectiles(){
        if (Gdx.input.isKeyJustPressed(Input.Keys.P)) {
            isPKeyPressed = true;
        } else if (!Gdx.input.isKeyPressed(Input.Keys.P)) {
            isPKeyPressed = false;
        }

        // Mettre à jour l'état du bouton droit de la souris
        if (Gdx.input.isButtonJustPressed(Input.Buttons.RIGHT)) {
            isRightMouseButtonPressed = true;
        } else if (!Gdx.input.isButtonPressed(Input.Buttons.RIGHT)) {
            isRightMouseButtonPressed = false;
        }
    }

    /**
     * Permet de lancer le projectile ou une fusie soit par le bouton droit(acteur)
     * ou gauche(enemie boss) de la souris
     * soit en appuyant sur la touche P(acteur) ou F(Boss enemie)
     */
    public void throwFusils(){
        if (Gdx.input.isKeyJustPressed(Input.Keys.F)) {
            isPKeyLeftPressed = true;
        } else if (!Gdx.input.isKeyPressed(Input.Keys.F)) {
            isPKeyLeftPressed = false;
        }
        // Mettre à jour l'état du bouton droit de la souris
        if (Gdx.input.isButtonJustPressed(Input.Buttons.LEFT)) {
            isLeftMouseButtonPressed = true;
        } else if (!Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
            isLeftMouseButtonPressed = false;
        }
    }

    //Detection des colision des missiles tiré par le boss
    public void checkCollisionFusilsToAttacker() {
        Iterator<Fusils> fusilsIterator = fusils.iterator();
            while (fusilsIterator.hasNext()) {
                Fusils fusils1 = fusilsIterator.next();
                if (Intersector.overlaps(fusils1.shape, attacker.shape)) {
                    fusilsIterator.remove();
                     attacker.setLife(attacker.getLife()-5);
                        break;
                    }
        }
    }


    //Affichaes des informations en haut de l'ecran
    public void showStatsKill(){
        font.draw(batch, "Score: " + scoreAttacker, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 10);
        font.draw(batch, "Level : " + currentLevel, 10, Gdx.graphics.getHeight() - 10);
        font.draw(batch, "Life : " + attacker.getLife() + " % ", Gdx.graphics.getWidth() - 100, Gdx.graphics.getHeight() - 10);
    }

    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
        shapeRenderer.dispose();
    }
    @Override
    public void show(){

    }
    @Override
    public void resize(int width, int height) {}

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void hide() {}

        /* if (scoreAttacker == 20) {
        currentLevel = HigherLevel.LEVEL;
        HigherLevel.setActive(true);
        LevelTwo.setActive(false);
        LevelOne.setActive(false);
        isShowingMessage = true;
        lastKillTime = TimeUtils.millis();
    }*/

}
