package com.mygdx.game.levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.actors.TomCat;
import com.mygdx.game.screen.KillScreen;
public class LevelOne {
    public static int LEVEL = 1;
    private static boolean active = true;

    public LevelOne(int level) {
        LEVEL = level;
    }

    //creatin d'un petit cible de guerre pour le niveau 1
    public static void createLittleTarget() {
        if (active) {
            TomCat petit = new TomCat(TomCat.PATH);
            petit.shape.x = MathUtils.random(0, Gdx.graphics.getWidth());
            petit.shape.y = Gdx.graphics.getHeight();
            petit.shape.width = 90;
            petit.shape.height = 90;
            KillScreen.tomCats.add(petit);
            KillScreen.lastKillTime = TimeUtils.nanoTime();
        }
    }

    public static boolean isActive() {
        return active;
    }

    public static void setActive(boolean isActive) {
        active = isActive;
    }
}

