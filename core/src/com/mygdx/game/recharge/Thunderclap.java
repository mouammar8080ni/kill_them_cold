package com.mygdx.game.recharge;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Thunderclap extends Recharge{

    public static final String TEXTURE_FILE_NAME = "Recharge/average.png";
    Texture texture = new Texture(Gdx.files.internal(TEXTURE_FILE_NAME));
    public Rectangle shape;
    private static String DEFAULT_NAME = "Thunderclap";
    private static int strength = 15;
    public Thunderclap(){
        super(strength, DEFAULT_NAME);
    }

    public void draw(SpriteBatch batch) {
        batch.draw(texture, shape.x, shape.y, shape.width, shape.height);
    }
}
