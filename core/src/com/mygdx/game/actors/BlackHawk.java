package com.mygdx.game.actors;

public class BlackHawk extends Target{

    public static final String PATH="target2.png";// Classe pour un cible moyen
    public String DEFAULT_NAME = "Black Hawk";
    public static int MAX_LIFE= 20;
    public BlackHawk(String path){
        super(path);
        setLife(MAX_LIFE);
        setName(DEFAULT_NAME);
    }

}
