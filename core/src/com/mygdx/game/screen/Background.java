package com.mygdx.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
public class Background {
    private Texture texture;
    private float x, y;
    private BitmapFont font;
    private float scrollSpeed;
    private float scrollY;
    private static boolean isAnimationEnabled;

    public Background(int x, int y, String path) {
        texture = new Texture(path);
        this.x = x;
        this.y = y;
        this.font = new BitmapFont();
        font.setColor(Color.RED);
        font.getData().setScale(2f);
        this.scrollSpeed = 60;
        this.scrollY = 0;
        isAnimationEnabled = true;
    }

    // Création de l'image de fond
    public void draw(SpriteBatch batch, float deltaTime) {
        if (isAnimationEnabled) {
            scrollY += scrollSpeed * deltaTime;
            if (scrollY > texture.getHeight()) {
                scrollY -= texture.getHeight();
            }
        }

        batch.draw(texture, x, y + scrollY);
        batch.draw(texture, x, y + scrollY - texture.getHeight());
    }

    //Permet de gerer l'animation
    public static void setAnimationEnabled(boolean animationEnabled) {
        isAnimationEnabled = animationEnabled;
    }

    public void dispose() {
        texture.dispose();
    }
}
