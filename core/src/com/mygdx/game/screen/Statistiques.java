package com.mygdx.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Align;
import com.mygdx.game.actors.Attacker;
import com.mygdx.game.actors.Boss;
import com.mygdx.game.actors.Target;

public class Statistiques {
    private ShapeRenderer shapeRenderer;
    private SpriteBatch spriteBatch;
    private BitmapFont font;
    Attacker attacker;
    Target target;
    private boolean isGameOver = true;
    public Statistiques() {
        attacker = new Attacker(Attacker.DEFAULT_NAME);
        target = new Target(Boss.PATH);
        shapeRenderer = new ShapeRenderer();
        spriteBatch = new SpriteBatch();
        font = new BitmapFont();
    }

    //Permet d'afficher un fond Gris avec le Level de passage au jeu
    public void draw() {
        if (!isGameOver) {
            return; // Ne rien dessiner si l'écran de message n'est pas visible
        }
        Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 1); // Gris clair
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.GRAY);
        shapeRenderer.rect(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        shapeRenderer.end();
        spriteBatch.begin();
        font.getData().setScale(2f);

        float centerX = Gdx.graphics.getWidth() / 2f;
        float baseY = Gdx.graphics.getHeight() / 2f;
        font.setColor(Color.YELLOW);
        font.draw(spriteBatch, "Height Scores : " + KillScreen.scoreAttacker, centerX, baseY + 90, 0, Align.center, false);
        font.draw(spriteBatch, "Life for : " + attacker.getLife(), centerX, baseY + 60, 0, Align.center, false);
        font.draw(spriteBatch, "Name Attacker : " + Attacker.DEFAULT_NAME, centerX, baseY + 30, 0, Align.center, false);
        font.draw(spriteBatch, "Name Targets : " + target.getName(), centerX, baseY, 0, Align.center, false);

        spriteBatch.end();
    }

    public void dispose() {
        isGameOver = false;
        shapeRenderer.dispose();
        spriteBatch.dispose();
        font.dispose();
    }

}

