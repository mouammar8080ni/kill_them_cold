package com.mygdx.game.recharge;

public class Recharge {

    private int strength;
    private String DEFAULT_NAME;

    //Cette classe est la classe mere pour les differents
    //recharge que l'acteur utilisera au fil du jeu
    public Recharge(int strength, String name){
        this.strength = strength;
        this.DEFAULT_NAME = name;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public String getDEFAULT_NAME() {
        return DEFAULT_NAME;
    }

    public void setDEFAULT_NAME(String DEFAULT_NAME) {
        this.DEFAULT_NAME = DEFAULT_NAME;
    }
}
