package com.mygdx.game.ammunition;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;

public class Projectiles {
    private Texture texture;
    private float x, y, yspeed;
    public Rectangle shape;
    public static final String PATH="missile.png";
    OrthographicCamera camera;

    /**
     * cette classe permet de generer un grand missile de
     * projectile dont l'impacte est de dimunition de -5% de vie des enemies touchées
     */
    public Projectiles(String path,float x,float y,float ySpeed){

        this.x = x;
        this.y = y;
        this.yspeed = yspeed;
        texture= new Texture(Gdx.files.internal(path));
        this.shape = new Rectangle(x,y,texture.getWidth(),texture.getHeight());
        camera= new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(),Gdx.graphics.getHeight());

    }

    public void draw(SpriteBatch batch) {
        batch.draw(texture, shape.x, shape.y, shape.width, shape.height);

    }

    public void moveProjectile(float delta) {
        shape.y+=  1000*Gdx.graphics.getDeltaTime();
        // Mettre à jour la position horizontale
    }
    public Rectangle getShape() {
        return shape;
    }
    public float getWidth(){
        return texture.getWidth();
    }
    public float getHeigth(){
        return texture.getHeight();
    }
    public void dispose(){
        texture.dispose();
    }


}

