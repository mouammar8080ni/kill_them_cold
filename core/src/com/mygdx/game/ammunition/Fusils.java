package com.mygdx.game.ammunition;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.utils.Array;


public class Fusils{

    private Texture texture;
    private float x, y, yspeed;
    public Rectangle shape;
    private Array<Projectiles> projectiles;
    public static final String PATH="missile.png";
    OrthographicCamera camera;
    public Fusils(String path,float x,float y,float ySpeed){

        this.x = x;
        this.y = y;
        this.yspeed = Math.abs(ySpeed);
        texture= new Texture(Gdx.files.internal(path));
        this.shape = new Rectangle(x,y,texture.getWidth(),texture.getHeight());
        camera= new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(),Gdx.graphics.getHeight());

    }
    public void draw(SpriteBatch batch) {
        batch.draw(texture, shape.x, shape.y, shape.width, shape.height);

    }
    public void moveFusil(float delta) {
        shape.y += yspeed * Gdx.graphics.getDeltaTime();
        // Mettre à jour la position horizontale
    }

    public Rectangle getShape() {
        return shape;
    }
    public float getWidth(){
        return texture.getWidth();
    }
    public float getHeigth(){
        return texture.getHeight();
    }
    public void dispose(){
        texture.dispose();
    }


}