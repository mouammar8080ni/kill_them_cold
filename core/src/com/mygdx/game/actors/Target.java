package com.mygdx.game.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.mygdx.game.ammunition.Ball;
import com.mygdx.game.screen.KillScreen;

import java.util.ArrayList;
import java.util.Iterator;


public class Target extends Actors{

    public static String name;
    private int life;
    public String path;
    private Texture texture;
    public Rectangle shape;
    OrthographicCamera camera;
    public float speedX=MathUtils.random(-6,6); // Vitesse de déplacement horizontal
    public float speedY=MathUtils.random(-6,6); // Vitesse de déplacement vertical
    public static ArrayList<Ball> balls_Targets = new ArrayList<>();
    public static ArrayList<Ball> balls = new ArrayList<>();// Tableau de minution qui seront generé sur different trajectoire
    public boolean test=true;

    public Target(String path) {
        super();
        this.path=path;
        texture = new Texture(Gdx.files.internal(path));
        shape = new Rectangle(0,0, texture.getWidth(), texture.getHeight());
        camera= new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(),Gdx.graphics.getHeight());

    }

    /**
     * Cette méthode permet de dessiner les positions de l'ennemie
     * @param batch
     */
    public void draw(SpriteBatch batch) {
        batch.draw(texture, shape.x, shape.y, shape.width, shape.height);

    }

    /**
     * Cette méthode permet de creer les balles de l'ennemie
     * et de l'ajouter dans une liste de  balls_targets
     */
    public void target_Ball(){
        Ball ball = new Ball(this.shape.x +this.shape.width/2,  this.shape.y, 5, -5);
        balls_Targets.add(ball);
    }


    /**
     * Cette methode verifie la collision des balls de l'ennemie avec l'acteur
     * Si une collision se produit, la balle est retirée de la collection
     * et on réduit -2 de la vie de l'acteur(attacker) et meurs si sa vien à 0
     * @param batch
     * @param attacker : l'acteur
     */
    public void checkCollisionAttacker(SpriteBatch batch, Attacker attacker){
        Iterator<Ball> ballIter = balls_Targets.iterator();
        while (ballIter.hasNext()) {
            Ball ball=ballIter.next();
            ball.update();
            ball.draw(batch);
            if(ball.getY()<0){
                ballIter.remove();
            }
            if(Intersector.overlaps(ball.getCirlce(),attacker.shape)) {
                ballIter.remove();
                attacker.setLife(attacker.getLife()-2);
            }

        }
    }

    /**
     * Celison des balles du boss avec l'attacker
     * @param batch
     * @param attacker : acteur
     */
    public void checkColisionBossToAttacker(SpriteBatch batch, Attacker attacker){
        Iterator<Ball> ballIterator = KillScreen.ball_boss.iterator();
        while (ballIterator.hasNext()) {
            Ball ball = ballIterator.next();
            ball.update();
            ball.draw(batch);
            if(ball.getY()<0){
                ballIterator.remove();
            }
            if(Intersector.overlaps(ball.getCirlce(),attacker.shape)) {
                attacker.setLife(attacker.getLife()-5);
                ballIterator.remove();
            }

        }

    }


    /**
     * cette méthode met à jour les positions de l'ennemie sur l'ecran
     * en les faisant bouger de maniere aleatoire avec des trajectoires
     * multidirectionnels avec une instence de Boss
     * @param delta : facteur de temps de lancement de l'ecran
     */
    public void moveTargets(float delta) {
        // Mettre à jour la position horizontale
        shape.x += speedX;

        // Mettre à jour la position verticale
        if(test){
            shape.y -=  200*Gdx.graphics.getDeltaTime();
        }else{
            shape.y +=  200*Gdx.graphics.getDeltaTime();
        }

        // Vérifier si l'ennemi est sorti de l'écran et le replacer de l'autre côté
        if(this.shape.y<=0){
            shape.y=0;
            speedY=-speedY;
            test=false;
        }
        if(this.shape.y>=Gdx.graphics.getHeight()){
            shape.y=Gdx.graphics.getHeight();
            test=true;
        }
        if (shape.x >= camera.viewportWidth - shape.width) {
            shape.x =camera.viewportWidth - shape.width;
            speedX=-speedX;
        } else if (shape.x <=0) {
            shape.x =0;
            speedX=-speedX;
        }

        if (shape.y > Gdx.graphics.getHeight()) {
            shape.y = -shape.height;
        } else if (shape.y <=Gdx.graphics.getHeight()/2 && this instanceof Boss) {

            test=false;
        }
    }

    /**
     * Cette méthode permet de créer de balls multidirectionnel et de les ajouter dans la collection balls
     */
    public void multidirectionalBalls(){
        Ball ball = new Ball(this.shape.x,  this.shape.y+this.shape.height/2, 5, 10);
        Ball ball1 = new Ball(this.shape.x+this.shape.width,  this.shape.y+this.shape.height/2, 5, -10);
        balls.add(ball);
        balls.add(ball1);
    }
    public int getLife() {
        return life;
    }
    public void setLife(int life) {
        this.life = life;
    }

    public float getWidth(){
        return texture.getWidth();
    }


    public String getName() {
        return name;
    }

    public  void setName(String name) {
        Target.name = name;
    }

    public float getHeigth(){
        return texture.getHeight();
    }

}
