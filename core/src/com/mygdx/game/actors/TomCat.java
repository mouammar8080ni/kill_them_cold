package com.mygdx.game.actors;

public class TomCat extends Target{
    private int MAX_LIFE = 15;
    public static final String PATH="target0.png"; // Classe pour un petit enemie
    public String DEFAULT_NAME = "Tom Cat";
    public TomCat(String path){
        super(path);
        setLife(MAX_LIFE);
        setName(DEFAULT_NAME);
    }

}
