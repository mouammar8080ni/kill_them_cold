package com.mygdx.game.levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Align;
import com.mygdx.game.animation.AudioMusic;
import com.mygdx.game.screen.KillScreen;

public class ShowLevel {
    private ShapeRenderer shapeRenderer;
    private SpriteBatch spriteBatch;
    private BitmapFont font;
    private boolean isMessageVisible = true;
    int level;
    public ShowLevel(int level) {
        shapeRenderer = new ShapeRenderer();
        spriteBatch = new SpriteBatch();
        font = new BitmapFont();
        this.level = level;
    }

    //Permet d'afficher un fond Gris avec le Level de passage au jeu
    public void draw() {
        if (!isMessageVisible) {
            return; // Ne rien dessiner si l'écran de message n'est pas visible
        }
        Gdx.gl.glClearColor(0.5f, 0.5f, 0.5f, 1); // Gris clair
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.GRAY);
        shapeRenderer.rect(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        shapeRenderer.end();
        spriteBatch.begin();
        font.getData().setScale(2f);
        font.draw(spriteBatch, "Level: "+ KillScreen.currentLevel, Gdx.graphics.getWidth() / 2f, Gdx.graphics.getHeight() / 2f, 0, Align.center, false);
        spriteBatch.end();
    }

    public void hide() {
        isMessageVisible = false;
        shapeRenderer.dispose();
        spriteBatch.dispose();
        font.dispose();
    }

}
