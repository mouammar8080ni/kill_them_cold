package com.mygdx.game.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;

public class Boss extends Target{

        // Classe pour le grand enemie avec une vie forte
        public static int MAX_LIFE = 50;
        public static final String PATH="boss.png";
        public String DEFAULT_NAME = "Boss";
        public Boss(String path){
            super(path);
            setLife(MAX_LIFE);
            setName(DEFAULT_NAME);
        }

}
