package com.mygdx.game.ammunition;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Circle;

public class Ball {

    // classe qui genere les balles tout au long de la guerre de Kill
    private float x, y, size, yspeed;
    private Circle circle;
    private Texture texture;

    public Ball(float x, float y, float size,float yspeed) {
        this.x = x;
        this.y = y;
        this.size = size;
        this.yspeed = yspeed;
        this.circle = new Circle();
        texture = new Texture("M.png");
    }

    public void update() {
        y += yspeed;
        if (y < 0) {
            yspeed = -yspeed;
        }

        circle.set(x,y, size/2);//Recuperation des coordonnés de la balles pour la colision
    }

    public void update1() {
        y += 5;
        x+=yspeed;

        if(x>= Gdx.graphics.getWidth()) {
            // Verrouillez le y de la balle vers le bas de l'écran
            x = Gdx.graphics.getWidth()-size;


            // Inversersement du ySpeed
            yspeed=-yspeed;
        }

        if(x<=0){
            x=0;
            yspeed=-yspeed;
        }

        circle.set(x,y, size/2);
    }

    public void draw(SpriteBatch batch) {
        batch.draw(texture,x, y, 10, 10);

    }

    public Circle getCirlce(){
        return this.circle;
    }

    public float getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public float getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

}
