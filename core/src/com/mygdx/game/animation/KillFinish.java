package com.mygdx.game.animation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.game.screen.Background;
import com.mygdx.game.screen.KillScreen;
import com.mygdx.game.screen.Statistiques;

public class KillFinish {

    private Texture restart;
    private Texture statsKill;
    private Statistiques statistiques;
    private boolean displayStats;// pour l'affichages des statitisque
    private boolean buttonClicked = false; // masquer le bouton statisque
    Texture buttonExit;// pour quitter apres la fin du jeu l'ecran



    /**
     * Cette classe creer un bouton qui permet de recomencer le jeu
     * si l'acteur a perdu, et qu'il veux un ouveau bataille de Kill In Cold
     */
    public KillFinish() {
        restart = new Texture(Gdx.files.internal("menu/reset.png"));
        statsKill = new Texture(Gdx.files.internal("menu/statistiques.png"));
        statistiques = new Statistiques();
        buttonExit = new Texture(Gdx.files.internal("menu/exit.png"));

    }

    public void draw(SpriteBatch batch) {
        //calcul du centre de l'ecran
        float centerX = Gdx.graphics.getWidth() / 2f;
        float centerY = Gdx.graphics.getHeight() / 2f;

        //Emplacement du bouton Reset egain au niveau du centre de l'ecran
        float restartX = centerX - restart.getWidth() / 2f;
        float restartY = centerY - restart.getHeight() / 2f + 40;
        batch.draw(restart, restartX, restartY);

        // Placement du bouton Statistiques en dessous du bouton Reset
        float buttonStatX = restartX - 55;//deplacer vers la gauche de l'axe 'x'
        float buttonStatY = restartY - statsKill.getHeight() - 70;
        batch.draw(statsKill, buttonStatX, buttonStatY);

        //Emplacement du bouton exit pour quitter l'ecran
        float buttonExitX = restartX - 60;
        float buttonExitY = restartY + buttonExit.getHeight() - 50;
        batch.draw(buttonExit, buttonExitX, buttonExitY);

        if (Gdx.input.isTouched()) {
            // Vérification du clic sur le bouton et lancer le jeu
            int touchX = Gdx.input.getX();
            int touchY = Gdx.graphics.getHeight() - Gdx.input.getY(); // Inversion de l'axe Y
            if (touchX >= restartX && touchX < restartX + restart.getWidth()
                    && touchY >= restartY && touchY < restartY + restart.getHeight()) {
                KillScreen.restartGame();
                Background.setAnimationEnabled(true);
            }

            else if (touchX >= buttonExitX && touchX < buttonExitX + buttonExit.getWidth()
                    && touchY >= buttonExitY && touchY < buttonExitY + buttonExit.getHeight()) {
                Gdx.app.exit(); // Quitter l'écran lorsque le bouton "Exit" est cliqué

                //Cette partie permetra d'afficher l'equipe qui à conçu ce jeu
            }

            //Cette partie permetra d'afficher les statistique apres la fin de jeu
            else if (touchX >= buttonStatX && touchX < buttonStatX + statsKill.getWidth() && touchY >= buttonStatY && touchY < buttonStatY + statsKill.getHeight()) {
                displayStats = true;

                // afficher les statisque pendant 5 Secondes
                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        displayStats = false;
                    }
                }, 5f);// delay de 5 secondes
            }
        }
        if (displayStats) {
            statistiques.draw();
        }

    }
}
