package com.mygdx.game.animation;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public class AudioMusic {
    public static Music music;

    //Cette Classe Gere le son tout au long du jeu
    public AudioMusic(String path){
        music = Gdx.audio.newMusic(Gdx.files.internal(path));// schemas du fichier à uploadé
    }

    public static void play(){
        music.play();
    }

    public static void stop(){
        music.stop();
    }

    public static void dispose(){
        music.dispose();
    }



}
