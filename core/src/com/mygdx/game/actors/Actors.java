package com.mygdx.game.actors;


public abstract class Actors {


    private int life;// Vies des cibles et d 'lacteur
    private int maxLife;

    /**
     * Une classe Abstract, mere de de l'acteur et de l'enemie
     */
    public Actors(){

    }

    public int getLife() {
        return life;
    }
    public void setLife(int life){
        this.life=life;
    }

    public int getMaxLife() {
        return maxLife;
    }

    public void setMaxLife(int maxLife) {
        this.maxLife = maxLife;
    }

    public boolean  isLive() {
        return this.life > 0 ;
    }


}

