package com.mygdx.game.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.ammunition.Ball;
import com.mygdx.game.exceptions.ImageNullException;
import com.mygdx.game.recharge.Microburst;
import com.mygdx.game.recharge.Thunderclap;
import com.mygdx.game.screen.KillScreen;
import com.badlogic.gdx.math.Rectangle;

import java.util.ArrayList;
import java.util.Iterator;

public class Attacker extends Actors{

    public static int MAX_LIFE = 100;
    private int maxScore = 100;
    public static String DEFAULT_NAME = "Iron Griffin";
    private OrthographicCamera camera;
    public static ArrayList<Ball> balls = new ArrayList<>();// Tableau de minution qui seront generé sur different trajectoire
    BitmapFont font;
    SpriteBatch batch;
    public static final String TEXTURE_FILE_NAME = "attacker.png" ;
    public static final Texture texture = new Texture(Gdx.files.internal(TEXTURE_FILE_NAME)) ;
    public Rectangle shape;

    public Attacker(String name) {
        super();
        shape = new Rectangle(0,0,texture.getWidth(), texture.getHeight());
        this.setMaxLife(MAX_LIFE);
        this.setLife(MAX_LIFE);
        font= new BitmapFont();
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800,400);

    }

    /**
     * Cette methode permet de dessiner la position de l'acteur
     * @param batch
     */
    public void draw(SpriteBatch batch)  throws ImageNullException {
        if(texture == null) throw new ImageNullException();
        batch.draw(texture, shape.x, shape.y, shape.width,shape.height);
    }


    /**
     * Cette méthode gere les deplacements de l'acteur avec la souris
     */
    public void moveWithMouse() {
        Vector3 touchPos = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
        camera.unproject(touchPos);

        // Déplacement horizontal de l'acteur en fonction de la souris
        shape.x = (int) touchPos.x ;

        // Déplacement vertical de l'acteur en fonction de la souris
        shape.y = (int) touchPos.y;

        Iterator<Ball> ballIterator = KillScreen.balls.iterator();
        while (ballIterator.hasNext()) {
            Ball ball = ballIterator.next();
            ball.setY((int)ball.getY() + 5);

            // Supprimer la balle si elle atteint la bordure supérieure
            if (ball.getY() >= Gdx.graphics.getHeight()) {
                ballIterator.remove();
            }
        }

        // Limiter le déplacement de l'acteur dans les limites dans l'ecran
        if (shape.x < 0) {
            shape.x = 0;
        }
        if (shape.x >= camera.viewportWidth - shape.width) {
            shape.x = camera.viewportWidth - shape.width;
        }
        if (shape.y < 0) {
            shape.y = 0;
        }
        if (shape.y >= camera.viewportHeight - shape.height) {
            shape.y = camera.viewportHeight - shape.height;
        }

    }

    /**
     * Cette méthode permet de créer de balls multidirectionnel et de les ajouter dans la collection balls
     */
    public void multidirectionalBalls(){
        Ball ball = new Ball(this.shape.x,  this.shape.y+this.shape.height/2, 5, 10);
        Ball ball1 = new Ball(this.shape.x+this.shape.width,  this.shape.y+this.shape.height/2, 5, -10);
        balls.add(ball);
        balls.add(ball1);
    }

    /**
     *cette methode consistera gerer les differents types
     * d'enemies qui herite de la classe mere par la generecité
     * @param spriteBatch
     * @param littleTarget
     */
    public void updateBalle(SpriteBatch spriteBatch, Array<? extends Target> littleTarget){
        Iterator <Ball> ballIterator= balls.iterator();
        while (ballIterator.hasNext()) {
            Ball ball= ballIterator.next();
            ball.update1();
            ball.draw(spriteBatch);

            // supression de petit enemie s'il atteint
            // la bordure de bas s'il ya pas eu de colision avoir une colision
            if( ball.getY()>=Gdx.graphics.getHeight()){
                ballIterator.remove();
                break;
            }

            Iterator <? extends Target> targetIterator = littleTarget.iterator();
            while(targetIterator.hasNext()){
                Target target=targetIterator.next();
                if(Intersector.overlaps(ball.getCirlce(),target.shape)){ // Detection de colision
                    KillScreen.scoreAttacker++;//augmentation du score de l'acteur
                    ballIterator.remove(); // impacte de balle avec l'enemie
                    targetIterator.remove();// Meurt de l'enemie
                }
            }

        }
    }

    /**
     * Permet de remetre la vie de l'acteur à MAx lorsque il depasse
     * le Max, quand il prend des recharges
     */
    public void maxLife(){
        if(this.getLife() >= MAX_LIFE);
            this.setLife(MAX_LIFE);
    }

    /**
     * Methode pour creer des petites recharges a l'acteur
     * qui servironts  d'augmentation de 10% de sa vie
     */
    public static void generateMicroburst(){
        Microburst microburst = new Microburst();
        microburst.shape.x = MathUtils.random(0, Gdx.graphics.getWidth());
        microburst.shape.y = Gdx.graphics.getHeight();
        microburst.shape.width = 80;
        microburst.shape.height = 80;
        KillScreen.microbursts.add(microburst);
        KillScreen.lastKillTime = TimeUtils.nanoTime();
    }


    /**
     * Permet de detecter si l'acteur a pris la recharge
     * et d'augment sa vie a 10%
     */
    public void checkCollisionRechargeMicroburst() {
        Iterator<Microburst> microburstIterator = KillScreen.microbursts.iterator();
        while (microburstIterator.hasNext()) {
            Microburst microburst1 = microburstIterator.next();
            microburst1.shape.y -= 200 * Gdx.graphics.getDeltaTime();
            microburst1.shape.x -= 10 * Gdx.graphics.getDeltaTime();
            if (microburst1.shape.y + microburst1.shape.height < 0) {
                microburstIterator.remove();
            }
            if(Intersector.overlaps(microburst1.shape,this.shape)) {
                microburstIterator.remove();
                this.setLife(this.getLife()+microburst1.getStrength());
                //si sa vie >= Max, on remet sa vie a 100% de MAX
                if(this.getLife() + microburst1.getStrength() > Attacker.MAX_LIFE){
                    this.maxLife();
                }

            }

        }
    }

    /**
     * Generere les recharge a tout les 5 Seconde
     */
    public void generateRecharge() {
        if (TimeUtils.timeSinceMillis(KillScreen.lastTime) > 5000) {
            generateMicroburst();
            KillScreen.lastTime = TimeUtils.millis();
        }
    }

    /**
     * Cette méthode gere les deplacements de l'acteur avec
     * avec les touche des directions du clavier
     */
    public void moveWithTouch() {
        float movementSpeed = 5f; // Vitesse de déplacement de l'attaquant

        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            shape.x += movementSpeed; // Déplacement vers la droite
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            shape.x -= movementSpeed; // Déplacement vers la gauche
        }
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            shape.y += movementSpeed; // Déplacement vers le haut
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            shape.y -= movementSpeed; // Déplacement vers le bas
        }
    }


    // Netoyer l'ecran
    public void dispose(){
        texture.dispose();
    }

    public int getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(int maxScore){
        this.maxScore = maxScore;
    }

    /**
     * Methode pour creer des petites recharges a l'acteur
     * qui servironts  d'augmentation de 10% de sa vie
     */
    public static void generateThunderclap(){
        Thunderclap thunderclap = new Thunderclap();
        thunderclap.shape.x = MathUtils.random(0, Gdx.graphics.getWidth());
        thunderclap.shape.y = Gdx.graphics.getHeight();
        thunderclap.shape.width = 80;
        thunderclap.shape.height = 80;
        KillScreen.thunderclaps.add(thunderclap);
        KillScreen.lastKillTime = TimeUtils.nanoTime();
    }

    /**
     * Permet de detecter si l'acteur a pris la recharge
     * et d'augment sa vie a 10%
     */
    public void checkCollisionRechargeThundercap() {
        Iterator<Thunderclap> thunderclapIterator = KillScreen.thunderclaps.iterator();
        while (thunderclapIterator.hasNext()) {
            Thunderclap thunderclap = thunderclapIterator.next();
            thunderclap.shape.y -= 200 * Gdx.graphics.getDeltaTime();
            thunderclap.shape.x -= 10 * Gdx.graphics.getDeltaTime();
            if (thunderclap.shape.y + thunderclap.shape.height < 0) {
                thunderclapIterator.remove();
            }
            if(Intersector.overlaps(thunderclap.shape,this.shape)) {
                thunderclapIterator.remove();
                this.setLife(this.getLife()+thunderclap.getStrength());
                //si sa vie >= Max, on remet sa vie a 100% de MAX
                if(this.getLife() + thunderclap.getStrength() > Attacker.MAX_LIFE){
                    this.maxLife();
                }

            }

        }
    }


}
