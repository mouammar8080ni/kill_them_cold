package com.mygdx.game.levels;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.actors.BlackHawk;
import com.mygdx.game.screen.KillScreen;

public class LevelTwo {

    public static int LEVEL = 2;
    private static boolean active = true;
    public LevelTwo(int level){
        LEVEL = level;
    }

    // un cible pour le niveau 2 et le niveau avec des
    //degats avancé et differents trajectoires
    public static void newFauconTarget() {
        if (active) {
            BlackHawk fauconNoir = new BlackHawk(BlackHawk.PATH);
            fauconNoir.shape.x = MathUtils.random(0, Gdx.graphics.getWidth());
            fauconNoir.shape.y = Gdx.graphics.getHeight();
            fauconNoir.shape.width = 150;
            fauconNoir.shape.height = 150;
            KillScreen.blackHawks.add(fauconNoir);
            KillScreen.lastKillTime = TimeUtils.nanoTime();
        }
    }

    public static boolean isActive() {
        return active;
    }

    public static void setActive(boolean isActive) {
        active = isActive;
    }
}
