package com.mygdx.game.exceptions;

public class TargetNullException extends Exception{
    public TargetNullException() {
        super("Tous les ennemis sont morts !!") ;
    }
}
